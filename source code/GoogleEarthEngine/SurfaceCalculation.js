///////////////////////////////////////////////////////////////////////////////////
// Supervised Land Use Classification in Misiones with Random Forest Algorithm
// Step 2 - Land Use Classification using Random Forest
///////////////////////////////////////////////////////////////////////////////////

/*
Authors:
- Carlos Brys
- Andrés Leszczuk
- National Geographic Institute (https://github.com/ign-argentina/mapa-nacional-cobertura-suelo/)
Project: Classification of coverages for the land use map in Misiones 
Period: second semester of the year 2023
Last updated: 15/03/2024
*/


// Asset Import
var WorkingImage = ee.Image("projects/mapas-de-misiones/assets/Working-Image-Misiones"),
    MisionesBoundary = ee.FeatureCollection("projects/mapas-de-misiones/assets/Misiones-shp"),
    MisionesWithoutUrbanAreas = ee.FeatureCollection("projects/mapas-de-misiones/assets/MisionesWithoutUrbanAreas-shp"),
    soil_samples = ee.FeatureCollection("projects/mapas-de-misiones/assets/usos_del_suelo_misiones") // http://u.osmfr.org/m/1035437/
;
// Clip the working image by the boundary of Misiones without urban areas
var WorkingImage = WorkingImage.clip(MisionesWithoutUrbanAreas);

var bands = ['B4_mean','B3_mean','B2_mean'];
var natural_color = {bands: bands, min: 0, max: 5000};

var bands2 = ['B4','B3','B2'];
var natural_color2 = {bands: bands2, min: 0, max: 5000};

// Bands to use in the classification
var selected_bands = ['NDVImean', 'NDVImin', 'NDVImax', 'NDVIrango', 'NDWImean', 'NDWImin', 'NDWImax', 'NDWIrango', 'BSImean', 'BSImin', 'BSImax', 'BSIrango', 'EVI2mean', 'EVI2min', 'EVI2max', 'EVI2rango', 'DEM', 'B2_mean', 'B3_mean', 'B4_mean', 'B8_mean', 'B11_mean', 'VH_mean', 'VV_mean', 'NDSImean', 'NDSImin', 'NDSImax', 'NDSIrango', 'SAVImean', 'SAVImin', 'SAVImax', 'SAVIrango', 'NDMImean', 'NDMImin', 'NDMImax', 'NDMIrango'];

// Extract the resulting image
var ClassifiedImage = WorkingImage.visualize({bands: selected_bands, min: 0, max: 0.3});

// Visualization of the working image without urban areas
Map.addLayer(WorkingImage, {}, 'Image to Classify');
Map.centerObject(WorkingImage, 8)


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// RANDOM FOREST Classification //////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Samples (Incorporate shape from "Assets" or take samples directly from GEE)
var samples = soil_samples.filterBounds(MisionesBoundary) 
var samples_1 = samples.randomColumn('random') // Assign a random column to these samples in order to later split them into 70/30

Export.table.toDrive(samples_1, 'Export_Samples')

// Samples Split
// 70% of the samples are separated for classification 
// and 30% for classifier validation
var TrainingSamples = samples_1.filter(ee.Filter.lt('random', 0.7))
var ValidationSamples = samples_1.filter(ee.Filter.gt('random', 0.3))

Export.table.toDrive(TrainingSamples, 'Download_Training')
Export.table.toDrive(ValidationSamples, 'Download_Validation')

var TrainingData = WorkingImage.sampleRegions({  
  collection: TrainingSamples,
  properties: ['class', 'random'],
  scale: 20 // before 30
})

// Bands to use in the classification
var selected_bands = ['NDVImean', 'NDVImin', 'NDVImax', 'NDVIrango', 'NDWImean', 'NDWImin', 'NDWImax', 'NDWIrango', 'BSImean', 'BSImin', 'BSImax', 'BSIrango', 'EVI2mean', 'EVI2min', 'EVI2max', 'EVI2rango', 'DEM', 'B2_mean', 'B3_mean', 'B4_mean', 'B8_mean', 'B11_mean', 'VH_mean', 'VV_mean', 'NDSImean', 'NDSImin', 'NDSImax', 'NDSIrango', 'SAVImean', 'SAVImin', 'SAVImax', 'SAVIrango', 'NDMImean', 'NDMImin', 'NDMImax', 'NDMIrango'];

// Train the RF Classifier
// The number indicates the amount of trees to be generated 
// and has an impact on the processing speed
var classifier = ee.Classifier.smileRandomForest(500) //   300
.train({
  features: TrainingData,
  classProperty: 'class',
  inputProperties: selected_bands
})

// Classify the image
var ClassifiedAreas = WorkingImage.classify(classifier)

/// Styles ///
// Category Colors
var ColorPalette = [
  'e0ff00',		//	1 Grassland
  '829308',		//	2 Scrubland
  '556c54',		//	3 Forest
  '006400',		//	4 Conifers
  '07edd1',		//	5 Eucalyptus
  'FFFFFF',		//	6 Tobacco
  '5fed07',		//	7 Tea
  '6B8E23',		//	8 Yerba Mate
  '61682f',		//	9 Araucaria
  'FFFFFF',		//	10 Corn
  'FFFFFF',		//	11 Sugar Cane
  'FFFFFF',		//	12 Cassava
  'FFFFFF',		//	13 Soybeans
  'FFFFFF',		//	14 Citrus
  'FFFFFF',		//	15 Pineapple
  'FFFFFF',		//	16 Bamboo
  '9B9B9B',		//	17 Soil
  '00BFFF',		//	18 Water
  'b3c346',    //	19 Meadow
  '00FFFF',    //	20 Wetland
  'BDB76B',    // 21 Kiri
//  '000000',    //	22 Urban Areas
  ];

// Category Names
var ClassNames = [
  ' 1 Grassland',
  ' 2 Scrubland',
  ' 3 Forest',
  ' 4 Conifers',
  ' 5 Eucalyptus',
  ' 6 Tobacco',
  ' 7 Tea',
  ' 8 Yerba Mate',
  ' 9 Araucaria',
  '10 Corn',
  '11 Sugar Cane',
  '12 Cassava',
  '13 Soybeans',
  '14 Citrus',
  '15 Pineapple',
  '16 Bamboo',
  '17 Soil',
  '18 Water',
  '19 Meadow',
  '20 Wetland',
  '21 Kiri',
//  '22 Urban Areas',
];

// Add the result to the map with the defined color palette in Styles (line 23)
Map.addLayer(ClassifiedAreas, {min:1, max: ClassNames.length, palette: ColorPalette}, 'RF Classification') 


// Confusion Matrix and Accuracy

var test = ClassifiedAreas.sampleRegions({
  collection: ValidationSamples,
  properties: ['class'],
  scale: 20
})
  
var testConfusionMatrix = test.errorMatrix('class', 'classification')

print('Confusion Matrix' , testConfusionMatrix);
print('RF Accuracy', testConfusionMatrix.accuracy());
print('Kappa Coefficient', testConfusionMatrix.kappa())

//////////////  Map Legends  /////////////////////////////////////////

var legend = ui.Panel({style: {position: 'bottom-left',padding: '8px 15px'}});
var legendTitle = ui.Label({value: 'Category', style: {fontWeight: 'bold',fontSize: '18px',margin: '0 0 4px 0',padding: '0'}
});
legend.add(legendTitle);
var makeRow = function(color, name) {
var colorBox = ui.Label({style: {backgroundColor: '#' + color,padding: '8px',margin: '0 0 4px 0'}
});

var description = ui.Label({value: name,style: {margin: '0 0 4px 6px'}});
return ui.Panel({widgets: [colorBox, description],layout: ui.Panel.Layout.Flow('horizontal')});
};


for (var i = 0; i < ClassNames.length; i++) {legend.add(makeRow(ColorPalette[i], ClassNames[i]));}  

Map.add(legend);


///////////////////////////////////// Export ///////////////////////////////////////////////////

/* 
var exportPath = 'projects/mapas-de-misiones/assets/';
 
Export.image.toAsset({
  image: ClassifiedAreas,
  assetId: exportPath + 'Land-Use-Classification-Misiones',
  region: MisionesBoundary,
  description: 'Land-Use-Classification-Misiones',
  scale: 20,
  maxPixels: 1e10
});


Export.image.toDrive({
  image: ClassifiedAreas,
  description: 'Land_Use_Classification_Misiones_2023',
  folder: 'Sentinel-2',
  scale: 20, 
  region: MisionesBoundary,
  maxPixels: 1e10 // 872093662, // If the export fails due to number of pixels, change it to the number indicated in this line. 
});

*/
