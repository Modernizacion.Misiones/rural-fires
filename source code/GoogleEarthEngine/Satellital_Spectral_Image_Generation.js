///////////////////////////////////////////////////////////////////////////////////
// Supervised Land Use Classification in Misiones with Random Forest Algorithm
// Step 1 - Image Generation
///////////////////////////////////////////////////////////////////////////////////

/*
Authors:
- Carlos Brys
- Andrés Leszczuk
- National Geographic Institute (https://github.com/ign-argentina/mapa-nacional-cobertura-suelo/)
- Subsecretariat of Territorial Planning. Ministry of Ecology and NNR
Project: Classification of coverages for the land use map in Misiones
Period: second semester of the year 2023
Last updated: 03/11/2024
*/

// Asset Import
var sar = ee.ImageCollection("COPERNICUS/S1_GRD"), // Sentinel SAR Images 1-GRD
    MisionesBoundary = ee.FeatureCollection("projects/mapas-de-misiones/assets/Misiones-shp"),
    DEM = ee.Image("USGS/SRTMGL1_003") // DEM from Google Earth Engine repository with 30m resolution
;

// Collection Visualization in natural color

var bands = ['B4_mean','B3_mean','B2_mean'];
var natural_color = {bands: bands, min: 0, max: 5000};

var bands2 = ['B4','B3','B2'];
var natural_color2 = {bands: bands2, min: 0, max: 5000};

//////////////////// Image Collection ///////////////////

// SENTINEL 2A/B - Preprocessing Level 1C

var s2c_01 = ee.ImageCollection("COPERNICUS/S2")
.filterDate('2023-06-01', '2023-12-31') 
.filterBounds(MisionesBoundary) 
.filterMetadata('CLOUDY_PIXEL_PERCENTAGE', 'less_than', 0.3); // Select Cloud Percentage

// SENTINEL 1 - Radar Images C band

var sar_vh = ee.ImageCollection(sar) // Double band of cross polarization, vertical transmission/horizontal reception
.filterDate('2023-06-01', '2023-12-31')
.filterBounds(MisionesBoundary)
.filter(ee.Filter.eq('instrumentMode', 'IW'))
.filter(ee.Filter.eq('orbitProperties_pass', 'DESCENDING'))
.select('VH').mean()
.rename('VH_mean')

var sar_vv = ee.ImageCollection(sar) // Band of polarization, vertical transmission/vertical reception
.filterDate('2023-06-01', '2023-12-31')
.filterBounds(MisionesBoundary)
.filter(ee.Filter.eq('instrumentMode', 'IW'))
.filter(ee.Filter.eq('orbitProperties_pass', 'DESCENDING'))
.select('VV').mean()
.rename('VV_mean')

/////////////////// Spectral Indices ////////////////////

// The values of the SENTINEL collection are reduced by the statistical values: mean, min, and max

// Mean
var s2c_2 = s2c_01.reduce(ee.Reducer.mean()).clip(MisionesBoundary);

// The image in natural collection is added to the map
Map.addLayer(s2c_2, natural_color, 'Natural Color'); 

// Min
var s2c_min = s2c_01.reduce(ee.Reducer.min())

// Max
var s2c_max = s2c_01.reduce(ee.Reducer.max())

// Indices are calculated from the previous values

// Normalized Difference Vegetation Index (NDVI) = (B8 - B4) / (B8 + B4)
var NDVI= s2c_2.normalizedDifference(['B8_mean','B4_mean']).rename('NDVImean')
var NDVImin = s2c_min.normalizedDifference(['B8_min', 'B8_min']).rename('NDVImin')
var NDVImax = s2c_max.normalizedDifference(['B8_max', 'B8_max']).rename('NDVImax')
var NDVIrango = NDVImax.subtract(NDVImin).rename('NDVIrango')

// Normalized Difference Water Index (NDWI) = (B8 - B3) / (B8 + B3)
var NDWI= s2c_2.normalizedDifference(['B3_mean','B8_mean']).rename('NDWImean')
var NDWImin = s2c_min.normalizedDifference(['B3_min','B8_min']).rename('NDWImin')
var NDWImax = s2c_max.normalizedDifference(['B3_max','B8_max']).rename('NDWImax')
var NDWIrango = NDWImax.subtract(NDWImin).rename('NDWIrango')

//Bare Soil Index (BSI) = (B11 + B4) – (B8 + B2) / (B11 + B4) + (B8 + B2)

// Mean
var T1_1= s2c_2.select('B11_mean');
var T1_2= s2c_2.select('B4_mean');
var T1_3= s2c_2.select('B8_mean');
var T1_4=s2c_2.select('B2_mean');
var b11addb4=T1_1.add(T1_2);
var b8addb2=T1_3.add(T1_4);
var term1=b11addb4.subtract(b8addb2);
var term2= b11addb4.add(b8addb2);
var BSI= term1.divide(term2).rename("BSImean");

// Min
var T2_1= s2c_min.select('B11_min');
var T2_2= s2c_min.select('B4_min');
var T2_3= s2c_min.select('B8_min');
var T2_4=s2c_min.select('B2_min');
var b11addb4_2=T2_1.add(T2_2);
var b8addb2_2=T2_3.add(T2_4);
var term1_2=b11addb4_2.subtract(b8addb2_2);
var term2_2= b11addb4_2.add(b8addb2_2);

var BSImin= term1_2.divide(term2_2).rename("BSImin");

// Max
var T3_1= s2c_max.select('B11_max');
var T3_2= s2c_max.select('B4_max');
var T3_3= s2c_max.select('B8_max');
var T3_4= s2c_max.select('B2_max');
var b11addb4_3=T3_1.add(T3_2);
var b8addb2_3=T3_3.add(T3_4);
var term1_3=b11addb4_3.subtract(b8addb2_3);
var term2_3= b11addb4_3.add(b8addb2_3);

var BSImax= term1_3.divide(term2_3).rename("BSImax");

//Range
var BSIrange = BSImax.subtract(BSImin).rename('BSIrange')

// Enhanced Vegetation Index (EVI2)
var EVI = s2c_2.expression(
  '2.4*((NIR-RED)/(NIR+RED+1))',{
    'NIR':s2c_2.select('B8_mean'),
    'RED':s2c_2.select('B4_mean'),
}).rename('EVI2mean'); 
  
// Min  
var EVImin = s2c_min.expression(
  '2.4*((NIR-RED)/(NIR+RED+1))',{
    'NIR':s2c_min.select('B8_min'),
    'RED':s2c_min.select('B4_min'),
}).rename('EVI2min');
  
// Max
var EVImax = s2c_max.expression(
  '2.4*((NIR-RED)/(NIR+RED+1))',{
    'NIR':s2c_max.select('B8_max'),
    'RED':s2c_max.select('B4_max'),
}).rename('EVI2max')

//Range
var EVIrange = EVImax.subtract(EVImin).rename('EVI2range')

// Digital Elevation Model (DEM)
var DEM = DEM.rename('DEM').clip(MisionesBoundary);

// Visible and Infrared Bands
var visibleinf = s2c_2.select(['B2_mean', 'B3_mean','B4_mean', 'B8_mean', 'B11_mean']);

// Normalized Difference Snow Index (NDSI) = (Green-SWIR) / (Green+SWIR)
var ndsi= s2c_2.expression(
  '(GREEN - SWIR ) / (GREEN  + SWIR )',{
    'GREEN':s2c_2.select('B3_mean'),
    'SWIR':s2c_2.select('B11_mean'),
}).rename('NDSImean');
  
// Min
var NDSImin= s2c_min.expression(
  '(GREEN - SWIR ) / (GREEN  + SWIR )',{
    'GREEN':s2c_min.select('B3_min'),
    'SWIR':s2c_min.select('B11_min'),
}).rename('NDSImin');
  
// Max
var NDSImax= s2c_max.expression(
  '(GREEN - SWIR ) / (GREEN  + SWIR )',{
    'GREEN':s2c_max.select('B3_max'),
    'SWIR':s2c_max.select('B11_max'),
}).rename('NDSImax');

// Range
var NDSIrange = NDSImax.subtract(NDSImin).rename('NDSIrange')

// Soil Adjusment Vegatation Index (SAVI) = [ (NIR - red ) / (NIR + red + L) ] ×(I+L)

// Mean
var savi= s2c_2.expression(
  'NIR-RED/(NIR+RED+0.5)*(1+0.5)',{
    'NIR':s2c_2.select('B8_mean'),
    'RED':s2c_2.select('B4_mean'),
}).rename('SAVImean'); 
  
// Min
var SAVImin= s2c_min.expression(
  'NIR-RED/(NIR+RED+0.5)*(1+0.5)',{
    'NIR':s2c_min.select('B8_min'),
    'RED':s2c_min.select('B4_min'),
}).rename('SAVImin'); 

// Max
var SAVImax= s2c_max.expression(
  'NIR-RED/(NIR+RED+0.5)*(1+0.5)',{
    'NIR':s2c_max.select('B8_max'),
    'RED':s2c_max.select('B4_max'),
}).rename('SAVImax'); 
  
// Range
var SAVIrange = SAVImax.subtract(SAVImin).rename('SAVIrange')

// Normalized Difference Moisture Index (NDMI) = (B08 - B11) / (B08 + B11)

// Mean
var NDMImean = s2c_2.normalizedDifference(['B8_mean','B11_mean']).rename('NDMImean')

// Min
var NDMImin = s2c_min.normalizedDifference(['B8_min', 'B11_min']).rename('NDMImin')

// Max
var NDMImax = s2c_max.normalizedDifference(['B8_max', 'B11_max']).rename('NDMImax')

// Range
var NDMIrange = NDMImax.subtract(NDMImin).rename('NDMIrange')

// Stack for classification
// An unique image is created with all the spectral bands, the calculated indices, and the DEM (36 layers)

var CompositeImage = ee.Image(NDVI)
.addBands(NDVImin)
.addBands(NDVImax)
.addBands(NDVIrange)
.addBands(NDWI)
.addBands(NDWImin)
.addBands(NDWImax)
.addBands(NDWIrango)
.addBands(BSI)
.addBands(BSImin)
.addBands(BSImax)
.addBands(BSIrange)
.addBands(visibleinf)
.addBands(sar_vh)
.addBands(DEM)
.addBands(sar_vv)
.addBands(ndsi)
.addBands(NDSImin)
.addBands(NDSImax)
.addBands(NDSIrange)
.addBands(EVI)
.addBands(EVImin)
.addBands(EVImax)
.addBands(EVIrange)
.addBands(savi)
.addBands(SAVImin)
.addBands(SAVImax)
.addBands(SAVIrange)
.addBands(NDMImean)
.addBands(NDMImin)
.addBands(NDMImax)
.addBands(NDMIrange)

/// Export ///

// Folder to export the image
var exportPath = 'projects/mapas-de-misiones/assets/';

Export.image.toAsset({
  image: CompositeImage,
  assetId: exportPath + 'Misiones-Working-Image',
  region: MisionesBoundary,
  description: 'Misiones-Working-Image',
  scale: 20,  // previously 30
  maxPixels: 1e10
});
