#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Language: Python 3
Purpose: Calculate the daily RFRI of the municipalities
Input file: MFI.csv (order,name,latitude,longitude,flammability_index)
Output file: FRFI.csv (latitude, longitude, RFRI)
@author: Carlos Brys
"""

import os
import pandas as pd
import csv
import requests
import math
from datetime import datetime, timedelta

os.system('clear')

API_KEY = "YSCHWQRR3RD4CTJMYCQWLW99C" 
# Please obtain your own API key for production
# Sign Up at https://www.visualcrossing.com/

# Read the CSV file
dataframe = pd.read_csv("MFI.csv") 

def get_visual_crossing_data(latitude, longitude, start_date, end_date):
    # response = requests.get("https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/-27.453335,-55.949062/last7days?key=P23LX4ZX8U85ZPVV4M8JHX4S2&elements=datetime,temp,precip,humidity,windspeed,pressure&maxDistance=48&include=days&unitGroup=metric")
    response = requests.get(f"https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/{latitude},{longitude}/{start_date}/{end_date}?key={API_KEY}&elements=datetime,temp,precip,humidity,windspeed,pressure&maxDistance=48&include=days&unitGroup=metric")
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Error getting data: {response.status_code}")
        return None

print("Processing input data from municipalities ... ")

# Create a CSV writer for the output file
# Open the CSV file in write mode
with open('RFRI.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    # Write the header row
    writer.writerow(['latitude', 'longitude', 'RFRI'])
    for row_index in range(dataframe.shape[0]):
        # Get latitude and longitude
        latitude = dataframe.loc[row_index, "latitude"]
        longitude = dataframe.loc[row_index, "longitude"]
        # Flammability Index
        flammability_index = dataframe.loc[row_index, "flammability_index"]
        # Get current date
        today = datetime.today()
        # Subtract 8 days from the current date
        start_date = today - timedelta(days=8)
        # Format the date to YYYY-MM-DD
        start_date = start_date.strftime("%Y-%m-%d")
        end_date = today.strftime("%Y-%m-%d")
        # Get data from Visual Crossing
        visual_crossing_data = get_visual_crossing_data(latitude, longitude, start_date, end_date)
        # Show data for the last 7 days
        if visual_crossing_data:
            # print("Start ", "*" * 20)
            rain_days = 10
            days = 0
            for day in visual_crossing_data["days"]:
                days = days + 1
                precipitation = day['precip']
                if precipitation > 5:  # it must rain more than 5mm to saturate the litter
                    last_rain_days = (today - pd.to_datetime(day['datetime'])).days
                #else:
                    # print("No rain that day")
                #print("-" * 20)
            wind_speed = day['windspeed']
            # Calculate Soil Moisture Factor
            ulr = last_rain_days
            x = today.timetuple().tm_yday
            ds = 2 * math.sin(2 * (math.pi) * ((x-93)/366)) + 4
            IMM = flammability_index
            FHS = math.sqrt(ulr/ds)
            FVV = wind_speed * 0.10  
            IMM = flammability_index
            FHS = math.sqrt(ulr/ds)
            FVV = wind_speed * 0.10 # FVV is 10% of the speed
            RFRI = int(IMM * FHS * FVV)
            # Write each line as it is completed
            writer.writerow([latitude, longitude, RFRI])
        else:
            print(f"No data found for location: {latitude}, {longitude}")
            print()

print("Process finished.")